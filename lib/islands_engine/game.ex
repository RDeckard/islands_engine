defmodule IslandsEngine.Game do
  use GenServer, start: {__MODULE__, :start_link, []}, restart: :transient
  alias IslandsEngine.{Application, Board, Coordinate, Guesses, Island, Rules}
  @players [:player1, :player2]
  @timeout 24 * 60 * 60 * 1000

  # API
  def start_link(name) when is_binary(name), do:
    GenServer.start_link(__MODULE__, name, name: via_tuple(name))

  def via_tuple(name), do: {:via, Registry, {Registry.Game, name}}

  def add_player(game, name) when is_binary(name), do:
    GenServer.call(game, {:add_player, name})

  def position_island(game, player, key, row, col) when player in @players, do:
    GenServer.call(game, {:position_island, player, key, row, col})

  def set_islands(game, player) when player in @players, do:
    GenServer.call(game, {:set_islands, player})

  def guess_coordinate(game, player, row, col) when player in @players, do:
    GenServer.call(game, {:guess_coordinate, player, row, col})

  # CALLBACK
  def init(name) do
    send(self(), {:set_state, name})
    {:ok, fresh_state(name)}
  end

  def terminate({:shutdown, :timeout}, state_data) do
    :dets.delete(Application.dets_file(), state_data.player1.name)
    :ok
  end
  def terminate(_reason, _state), do: :ok

  def handle_call({:add_player, name}, _from, state_data) do
    with {:ok, rules} <- Rules.check(state_data.rules, :add_player)
    do
      reply_success(:ok, state_data |> update_player2_name(name) |> update_rules(rules))
    else
      :error -> reply_error(:error, state_data)
    end
  end

  def handle_call({:position_island, player, key, row, col}, _from, state_data) do
    board = player_board(state_data, player)
    with  {:ok, rules} <-
            Rules.check(state_data.rules, {:position_islands, player}),
          {:ok, coordinate} <-
            Coordinate.new(row, col),
          {:ok, island} <-
            Island.new(key, coordinate),
          %{} = board <-
            Board.position_island(board, key, island)
    do
      reply_success(:ok, state_data |> update_board(player, board) |> update_rules(rules))
    else
      error when error in [
          :error,
          {:error, :invalid_coordinate},
          {:error, :invalid_island_type}
        ] -> reply_error(error, state_data)
    end
  end

  def handle_call({:set_islands, player}, _from, state_data) do
    board = player_board(state_data, player)
    with  {:ok, rules} <-
            Rules.check(state_data.rules, {:set_islands, player}),
          true <-
            Board.all_islands_positioned?(board)
    do
      reply_success({:ok, board}, state_data |> update_rules(rules))
    else
      :error -> reply_error(:error, state_data)
      false  -> reply_error({:error, :not_all_islands_positioned}, state_data)
    end
  end

  def handle_call({:guess_coordinate, player, row, col}, _from, state_data) do
    opponent = opponent(player)
    opponent_board = player_board(state_data, opponent)
    with  {:ok, rules} <-
            Rules.check(state_data.rules, {:guess_coordinate, player}),
          {:ok, coordinate} <-
            Coordinate.new(row, col),
          {hit_or_miss, forested_island, win_status, opponent_board} <-
            Board.guess(opponent_board, coordinate),
          {:ok, rules} <-
            Rules.check(rules, {:win_check, win_status})
    do
      reply_success(
        {:ok, {hit_or_miss, forested_island, win_status}},
        state_data
        |> update_board(opponent, opponent_board)
        |> update_guesses(player, hit_or_miss, coordinate)
        |> update_rules(rules))
    else
      error when error in [:error, {:error, :invalid_coordinate}] ->
        reply_error(error, state_data)
    end
  end

  def handle_info(:timeout, state_data), do:
    {:stop, {:shutdown, :timeout}, state_data}

  def handle_info({:set_state, name}, _state_data) do
    state_data =
    case :dets.lookup(Application.dets_file(), name) do
      []              -> fresh_state(name)
      [{_key, state}] -> state
    end
    :dets.insert(Application.dets_file(), {name, state_data})
    {:noreply, state_data, @timeout}
  end


  # PRIVATE
  defp reply_success(reply, state_data) do
    :dets.insert(Application.dets_file(), {state_data.player1.name, state_data})
    {:reply, reply, state_data, @timeout}
  end

  defp reply_error(reply, state_data) do
    {:reply, reply, state_data, @timeout}
  end

  defp fresh_state(name) do
    player1 = %{name: name, board: Board.new(), guesses: Guesses.new()}
    player2 = %{name: nil, board: Board.new(), guesses: Guesses.new()}
    %{player1: player1, player2: player2, rules: %Rules{}}
  end

  defp player_board(state_data, player), do: Map.get(state_data, player).board
  defp opponent(:player1), do: :player2
  defp opponent(:player2), do: :player1

  defp update_player2_name(state_data, name), do:
    put_in(state_data.player2.name, name)

  defp update_rules(state_data, rules), do: %{state_data | rules: rules}

  defp update_board(state_data, player, board), do:
    Map.update!(state_data, player, fn player -> %{player | board: board} end)

  defp update_guesses(state_data, player_key, hit_or_miss, coordinate) do
    update_in(state_data[player_key].guesses, fn guesses ->
      Guesses.add(guesses, hit_or_miss, coordinate)
    end)
  end
end
